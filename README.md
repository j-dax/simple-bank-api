## Functional Requirements

| Req ID | Description |
| :---: | :---: |
| FN-0 | A client create new accounts of various categories. |
| FN-1 | Clients can deposit funds |
| FN-2 | Clients can withdraw funds from the account  |
| FN-3 | A client may close out accounts. |
| FN-API-OBJNOTFOUND | Any request that does not create an object and fails to find the requested object returns a 404 status. |

1. All endpoints listed below must have a Postman test verifying functionality
2. Data is stored in a database.
3. Data Access is performed through the use of Data Access Objects.
4. All input is sent from a client (Postman) and handled by the Server
5. Logging is implemented throughout the application
6. All DAO and Service methods must have a test proving that they work

| FN-API0 | An API for creating and viewing clients must exist under /clients |
| :---: | :---: |
| FN-API0-GET | A GET request to this interface returns all clients and responds with a 200 status. |
| FN-API0-POST | A POST request to this interface generates a client and responds with a 201 status. |

| FN-API1 | An API for viewing, updating and deleting clients must exist under /clients/<int: client_id> |
| :---: | :---: |
| FN-API1-GET | A GET request to this interface returns the client under a 200 status if the client is found. See also FN-API-OBJNOTFOUND. |
| FN-API1-PUT | A PUT request to this interface updates a client if it exists. See also FN-API-OBJNOTFOUND. |
| FN-API1-DELETE | A DELETE request to this interface deletes the client referenced by `client_id` if it exists. On success, it returns a 205 status. See also FN-API-OBJNOTFOUND. |

| FN-API2 | An API for client account creation and viewing must exist under /clients/<int: client_id>/accounts |
| :---: | :---: |
| FN-API2-POST | A POST request to this interface creates a new account for the client referenced by `client_id` returning a 201 status. |
| FN-API2-GET | A GET request to this interface returns all accounts associated with `client_id`. See also FN-API-OBJNOTFOUND. **Note that a client with no accounts SHOULD NOT 404.* |
| FN-API2-GET | A GET request may contain additional predefined parameters to filter accounts. |

| FN-API3 | An API for accessing client accounts must exist under /clients/<int: client_id>/accounts/<int: account_id> |
| :---: | :---: |
| FN-API3-GET | Search for and return an account with the same `account_id` for the same client with `client_id` if it exists. See also FN-API-OBJNOTFOUND. |
| FN-API3-PUT | Search for an account with the same `account_id` for the same client with `client_id`. If found, update that account. See also FN-API-OBJNOTFOUND. |
| FN-API3-DELETE | Search for an account with the same `account_id` for the same client with `client_id`. If found, delete that account. See also FN-API-OBJNOTFOUND. |
| FN-API3-PATCH | Search for an account with the same `account_id` for the same client with `client_id`. If found, increase the balance with a "deposit" and decrease the balance with a "withdraw". At least one of "deposit" or "withdraw" will be found in the body. Return 422 status if funds would become negative. See also FN-API-OBJNOTFOUND. |

| FN-API4 | An API for ******* clients must exist under /clients/<int: client_id>/accounts/<int: from_account_id>/transfer/<int: to_account_id> |
| :---: | :---: |
| FN-API4-PATCH | Search for two accounts, `from_account_id` and `to_account_id`, where the client with `client_id` owns `from_account_id`. If found, decrease the balance of `from_account_id` by "amount" and increate the balance of `to_account_id` with by the same amount. "amount" must be found in the body. Return 422 status if funds would become negative. See also FN-API-OBJNOTFOUND. |

## Nonfunctional Requirements

| Req ID | Description |
| :---: | :---: |
| NF-LOG | Logging is implemented throughout the application |
| NF-ENDP | All endpoints must have a Postman test verifying functionality |
| NF-CSA | All input is sent from a client (Postman) and handled by the Server |
| NF-DB | Data is stored in a database. |
| NF-DAO | Data Access is performed through the use of Data Access Objects. |
| NF-TEST | All DAO and Service methods must have a test proving that they work |
